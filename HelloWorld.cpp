﻿#include <iostream>

//Вызывает функцию std::cout
void print()
{
    // Потому что хотим использовать стандарную библиотеку для печати в консоль и
    // не использовать какие либо другие библиотеки
    std::cout << "Hello World";
}

/*
* Прописываем значение переменной и печатые Hello World
*/

int main()
{
    int x = 100;
    std::cout << 7 + 3 << std::endl;
    int y = x + 100;

    int b = 0;
    b = b + 2;

    int mult = x * y;
    std::cout << mult << std::endl;
    print();
}